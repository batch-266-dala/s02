package com.zuitt.example;

import java.util.Scanner;

public class Exercise {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String result;
        System.out.println("Enter year: ");
        int year = input.nextInt();


        if(year % 400 == 0) {
            result = " is a leap year";
        } else if (year % 100 == 0) {
            result = " is NOT a leap year";
        } else if (year % 4 == 0) {
            result = " is a leap year";
        } else {
            result = " is NOT a leap year";
        }

        System.out.println(year + result);

    }
}
