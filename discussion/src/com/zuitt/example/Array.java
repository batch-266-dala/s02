package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {
//       Array Syntax:
//        dataType[] identifier = new dataType[numOfElements];

        int[] intArray = new int[5];
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 99;


        System.out.println(Arrays.toString(intArray));

//        String Array with initialization
//        Syntax:
//        dataType[] identifier = {elementA, elementB, elementC}
        String[] names = {"John", "Jane", "Joe"};
        System.out.println(Arrays.toString(names));

//        Java Array Method
//        1. Sort
        Arrays.sort(intArray);
        System.out.println("Order of items after sort() method " + Arrays.toString(intArray));

        String[][] classroom = new String[3][3];
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        classroom[1][0] = "Brandon";
        classroom[1][1] = "Mike";
        classroom[1][2] = "Jeremy";
        classroom[2][0] = "Rydon";
        classroom[2][1] = "Ekans";
        classroom[2][2] = "Mitos";

        System.out.println(Arrays.deepToString(classroom));

//        ArrayList/Objects in Javascript
//        - resizable arrays, wherein elements can be added or removed
//        Syntax:
//        ArrayList<T> identifier = new ArrayList<T>();
        ArrayList<String> students = new ArrayList<String>(Arrays.asList("Jane", "Mike"));

//        Add elements
        students.add("John");
        students.add("Ion");
        students.add("Vento");

        System.out.println(students);
//        Get element
        System.out.println(students.get(0));

//        Updating element
        students.set(0, "updated Name");

        System.out.println(students);

//        Removing element
        students.remove(0);
        System.out.println(students);

//        Delete all elements
        students.clear();
        System.out.println(students);

//        Hashmaps
//        Syntax:
//        Hashmap<T, T> identifier = new HashMap<T,T>();
        HashMap<String, Integer> jobPosition = new HashMap<>();

//        Adding elements to Hashmap
        jobPosition.put("Miya", 5);
        jobPosition.put("Eson", 6);

        System.out.println(jobPosition);

//        Get element
        System.out.println(jobPosition.get("Miya"));

//        Get all elements
        System.out.println(jobPosition.values());

//        Clear all elements
        jobPosition.clear();
        System.out.println(jobPosition);








    }
}
