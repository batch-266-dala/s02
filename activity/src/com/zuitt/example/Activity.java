package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity {
    public static void main(String[] args) {
//        Prime Numbers
        int[] prime = new int[5];
        prime[0] = 2;
        prime[1] = 3;
        prime[2] = 5;
        prime[3] = 7;
        prime[4] = 11;

        System.out.println("The first prime number is: " + prime[0]);

//        Friend Lists
        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("Eson", "Noah", "Suga", "Eri"));
        System.out.println("My friends are: " + friends);

//        Inventory
        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);




    }

}
