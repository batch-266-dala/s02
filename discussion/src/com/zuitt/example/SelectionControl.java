package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {
//        Java Operators
//        Arithmetic: +, -, *, /, %
//        Comparison: >, <, >=, <=, ==, !=
//        Logical: &&, ||, !
//        Assignment: =
        int num = 36;
//      If else statement
        if(num % 5 == 0) {
            System.out.println(num + " is divisible by 5.");
        } else {
            System.out.println(num + " is not divisible by 5.");
        }

//        Ternary Operator
        int grade = 90;
        Boolean result = (grade >= 75) ? true : false;
        System.out.println(result);

//        Switch Case
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int directionValue = numberScanner.nextInt();

        switch(directionValue) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid input");
        }


    }
}
